---
layout: page
title: About
---

This micro-site is where I collect information about embedded development and
the testing issues around it.  For more about me and what I do, check out [my
blog](http://claydowling.com).
