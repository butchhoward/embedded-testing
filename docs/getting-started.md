---
title: "Getting Started"
layout: page
---

The workshop is meant to be run using GitLab's WebIDE.  You need nothing on your workstation other than a modern browser.

## Instructions

1. Log in to your [GitLab](https://gitlab.com) account.  Create a new account if you don't have one.

1. Go to the [Embedded Testing](https://gitlab.com/ClayDowling/embedded-testing) repository.

1. **FORK THIS REPOSITORY**  You don't have commit rights to my repository, so you need your own copy.

1. Open the WebIDE.

